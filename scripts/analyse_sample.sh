
if [ "$#" -eq 1 ]

then    
    sampleid=$1
    
    echo "Running FastQC..."
    mkdir -p out/fastqc
    fastqc -o out/fastqc data/${sampleid}*.fastq.gz
    echo

#Visual block, shift + I sin quitarlo (x en visualblock para eliminar)
## Si se quisiera correr seqtk    
#    echo "Running seqtk..."
#    semilla=1
#    num_lecturas=300000
#    mkdir -p data/original
#    mv data/${sampleid}*.fastq.gz data/original/
#    for dato in {1..2}
#    do
#        seqtk sample -s$semilla /data/original/${sampleid}_${dato}.fastq.gz $num_lecturas | \
#        gzip > data/${sampleid}_${dato}.fastq.gz 
#    done

    echo "Running cutadapt..."
    mkdir -p log/cutadapt
    mkdir -p out/cutadapt
    adapt_init=AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
    adapt_fin=AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
    cutadapt -m 20 -a $adapt_init -A $adapt_fin \
    -o out/cutadapt/${sampleid}_1.trimmed.fastq.gz -p out/cutadapt/${sampleid}_2.trimmed.fastq.gz \
    data/${sampleid}_1.fastq.gz data/${sampleid}_2.fastq.gz > log/cutadapt/${sampleid}.log
    echo

    echo "Running STAR alignment..."
    mkdir -p out/star/${sampleid}
    STAR --runThreadN 4 --genomeDir res/genome/star_index/ \
    --readFilesIn out/cutadapt/${sampleid}_1.trimmed.fastq.gz out/cutadapt/${sampleid}_2.trimmed.fastq.gz \
    --readFilesCommand zcat --outFileNamePrefix out/star/${sampleid}/
    echo
    echo "Sample $sampleid analysis completed"
    echo

else
    echo "Usage: $0 <sampleid>"
    exit 1
fi
