
echo "Creating new conda environment..."

env_name=testsim

conda create -n $env_name
conda activate $env_name
conda install fastqc star multiqc
#conda install seqtk
mkdir envs
conda $env_name export > envs/rna-seq.yaml

echo "Done"
