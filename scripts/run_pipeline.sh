#Ejecutardesde directorio general, no /scripts
#Comando para ejecutar: bash run_pipeline.sh &> log/run_pipeline.out

#Paquetes necesarios para análisis: fastqc star multiqc (seqtk)

echo "Initializing..."
echo

echo "Creating environment..."
#Entendemos que la carpeta /scripts está ya hecha
mkdir -p data log out res/genome 
echo

#echo "Obtaining data..."
#data_domain=
#wget -P $(pwd)/data data_domain
#
#echo

echo "Obtainig genome..."
genome_name=ecoli
genome_domain=ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz

wget -O res/genome/$genome_name.fasta.gz $genome_domain
gunzip -k res/genome/$genome_name.fasta.gz
echo

echo "Running STAR index for $genome_name..."
mkdir -p res/genome/star_index
STAR --runThreadN 4 --runMode genomeGenerate \
--genomeDir res/genome/star_index  \
--genomeFastaFiles res/genome/$genome_name.fasta \
--genomeSAindexNbases 9
echo

for sample_id in $(ls data/*.fastq.gz | cut -d "_" -f1 | sed 's:data/::' | sort | uniq)
do
    echo "Analysing sample $sample_id..."
    echo    
    bash scripts/analyse_sample.sh $sample_id
    echo

done

echo "Running multiQC..."
multiqc -o out/multiqc $(pwd)
echo

echo "Pipeline finished."
